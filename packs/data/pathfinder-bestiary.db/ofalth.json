{
    "_id": "jP8CO6z7bNIhOuqQ",
    "data": {
        "abilities": {
            "cha": {
                "mod": -2
            },
            "con": {
                "mod": 6
            },
            "dex": {
                "mod": 3
            },
            "int": {
                "mod": -2
            },
            "str": {
                "mod": 7
            },
            "wis": {
                "mod": 2
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 31
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 170,
                "temp": 0,
                "value": 170
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 18
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [],
                "value": "30"
            }
        },
        "details": {
            "alignment": {
                "value": "CE"
            },
            "blurb": "",
            "creatureType": "Aberration",
            "level": {
                "value": 10
            },
            "privateNotes": "",
            "publicNotes": "<p>Found in castle dung heaps, city dumps, and sewers, ofalths are thought to be cousins of shamblers. But whereas shamblers are living heaps of soggy vegetation, ofalths are living heaps of matter from an altogether more unpleasant source: these monsters look like 9-foot-tall amalgamations of wet detritus, sewage, and rubbish with long tentacular arms and stout legs. It can be difficult to tell where an ofalth's body ends and the foul contents of the cesspit they wallow within begins. They move through refuse heaps in search of organic material in their endless quest to sate their hunger.</p>\n<p>Though ofalths have a limited intellect, they still exhibit a vile curiosity. They are no strangers to dissecting their prey after it has succumbed to their wasting disease-a terrible and aptly named affliction that causes the victim's blood to seep from its pores. When an ofalth manages to secure a victim alive, it may even torture its food by consuming it bit by bit while it shrieks for mercy.</p>\n<p>Ofalths' domains typically overlap with those of otyughs, who fear ofalths, though otyughs have also been known to band together in order to take down an ofalth for its rubbery flesh, which the otyughs consider an intoxicating delicacy. On the other hand, ofalths seem to particularly enjoy taking otyughs apart one piece at a time. They have been known to keep a captured otyugh alive for days or even weeks, forcing the otyugh to endure the cloying scent of its own body decaying before the ofalth feeds.</p>",
            "source": {
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 22
            },
            "reflex": {
                "saveDetail": "",
                "value": 17
            },
            "will": {
                "saveDetail": "",
                "value": 18
            }
        },
        "traits": {
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "disease",
                    "poison"
                ]
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "common"
                ]
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "lg"
            },
            "traits": {
                "custom": "",
                "value": [
                    "aberration"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "wR8rvUNTkSFlNhUv",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": [
                        "wretched-weeps"
                    ]
                },
                "bonus": {
                    "value": 23
                },
                "damageRolls": {
                    "0vxbg2sh3l540xbk674e": {
                        "damage": "2d12+13",
                        "damageType": "bludgeoning"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "reach-10"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Fist",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "Vdx4lOd9PGzzo18M",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": [
                        "wretched-weeps"
                    ]
                },
                "bonus": {
                    "value": 19
                },
                "damageRolls": {
                    "f4nu8izy7ubgs5hswbxd": {
                        "damage": "2d10+7",
                        "damageType": "bludgeoning"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "range-increment-30"
                    ]
                },
                "weaponType": {
                    "value": "ranged"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Fling Offal",
            "sort": 200000,
            "type": "melee"
        },
        {
            "_id": "3A8TmAuVP2C3XDsX",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>When it's not in danger, an ofalth can spend 1 minute settling into a 10-foot pile that looks like a heap of garbage. While doing so, the ofalth gains a +2 circumstance bonus to AC but can't use attack, manipulate, or move actions. A creature that enters the area of the garbage heap or interacts with it must attempt a save against the ofalth's putrid stench and wretched weeps disease. An ofalth can leave this form using a single action.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Refuse Pile",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "h3RAiOfN4DCwKqV0",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>30 feet. A creature entering the aura must succeed at a <span data-pf2-check=\"fortitude\" data-pf2-dc=\"28\" data-pf2-traits=\"aura\" data-pf2-label=\"Putrid Stench DC\" data-pf2-show-dc=\"gm\">Fortitude</span> save or become sickened 1 until the end of its turn (plus slowed 1 for as long as it is sickened on a critical failure). While within the aura, an affected creature takes a -2 circumstance penalty to saves against disease and to recover from the sickened condition. A creature that succeeds at its save is temporarily immune for 1 minute.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "aura"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Putrid Stench",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "CDkktCbVeomEuX4D",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A ofalth gains fast healing 2 when in an area with a high concentration of debris, junk, or excrement, such as a refuse heap or sewer.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Filth Wallow",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "qcaLKbJ4n5L9k9Vc",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p><strong>Saving Throw</strong> <span data-pf2-check=\"fortitude\" data-pf2-dc=\"26\" data-pf2-traits=\"disease\" data-pf2-label=\"Wretched Weeps DC\" data-pf2-show-dc=\"gm\">Fortitude</span></p>\n<p><strong>Stage 1</strong> carrier with no ill effect (1 day)</p>\n<p><strong>Stage 2</strong> [[/r {2d8}[persistent,bleed]]] @Compendium[pf2e.conditionitems.Persistent Damage]{Persistent Bleed Damage} every hour and @Compendium[pf2e.conditionitems.Enfeebled]{Enfeebled 1} (1 day)</p>\n<p><strong>Stage 3</strong> [[/r {2d8}[persistent,bleed]]] @Compendium[pf2e.conditionitems.Persistent Damage]{Persistent Bleed Damage} every hour and @Compendium[pf2e.conditionitems.Enfeebled]{Enfeebled 2} (1 day)</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "disease"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Wretched Weeps",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "Q2TToiZaW4GBsEjH",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 23
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 700000,
            "type": "lore"
        },
        {
            "_id": "WWQwqsaUXwuRwu4b",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 19
                },
                "proficient": {
                    "value": 0
                },
                "rules": [
                    {
                        "key": "FlatModifier",
                        "label": "In Trash and Rubbish",
                        "predicate": {
                            "any": [
                                "terrain:trash",
                                "terrain:rubbish"
                            ]
                        },
                        "selector": "stealth",
                        "value": 4
                    }
                ],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "variants": {
                    "0": {
                        "label": "+23 in trash and rubbish",
                        "options": "terrain:trash, terrain:rubbish"
                    }
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 800000,
            "type": "lore"
        }
    ],
    "name": "Ofalth",
    "token": {
        "disposition": -1,
        "height": 2,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Ofalth",
        "width": 2
    },
    "type": "npc"
}
